package com.sarmady.carsmachinery.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.activities.SettingsActivity;
import com.sarmady.carsmachinery.activities.SplashActivity;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuFragment extends Fragment {
    @BindView(R.id.btn_facebook_logout)
    Button mLogoutButton;
    @BindView(R.id.btn_settings)
    Button mSettingsButton;
    @BindView(R.id.iv_facebook_profile)
    ProfilePictureView mProfileImageView;
    @BindView(R.id.tv_facebook_username)
    TextView mUsernameTextView;
    @BindView(R.id.tv_facebook_email)
    TextView mEmailTextView;


    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]
    private FirebaseUser currentUser;
    private Context mContext;
    @BindView(R.id.adview_mpu)
    AdView mMPUAdView;

//    private FirebaseAuth mAuth;
//    private ProfileTracker mProfileTracker;


    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = Objects.requireNonNull(getActivity()).getApplicationContext();

        mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();
        if (Profile.getCurrentProfile() == null) {
            signOut();
        }else{
            mProfileImageView.setProfileId(Profile.getCurrentProfile().getId());
            mUsernameTextView.setText(Profile.getCurrentProfile().getName());
            mEmailTextView.setText(currentUser.getEmail());
        }

        MobileAds.initialize(mContext,
                getString(R.string.app_ad_id));


        AdRequest adRequest = new AdRequest.Builder()/*.addTestDevice("C47FB4C7CA3EC78196EB786392DE443C")*/.build();
        mMPUAdView.loadAd(adRequest);


        mSettingsButton.setOnClickListener(v ->
        {
            Intent intent = new Intent(getActivity().getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        });
        mLogoutButton.setOnClickListener(v -> signOut());
//        mEmailTextView.setText();

    }

    public void signOut() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        Intent intent = new Intent(Objects.requireNonNull(getActivity()).getApplicationContext(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }

}
