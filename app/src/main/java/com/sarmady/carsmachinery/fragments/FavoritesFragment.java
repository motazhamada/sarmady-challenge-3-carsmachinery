package com.sarmady.carsmachinery.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.adapters.CarsRecyclerViewAdapter;
import com.sarmady.carsmachinery.classes.CarItem;
import com.sarmady.carsmachinery.classes.CarsList;
import com.sarmady.carsmachinery.utilities.SavePrefs;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoritesFragment extends Fragment {

    @BindView(R.id.rv_cars_favorites)
    RecyclerView mCarsRecyclerView;
    @BindView(R.id.pb_cars_favorites)
    ProgressBar mCarsProgressBar;
    @BindView(R.id.tb_favorites)
    Toolbar mToolbar;
    @BindView(R.id.cl_cars_favorites)
    ConstraintLayout mLayout;
    @BindView(R.id.adView_banner)
    AdView mAdView;
    private CarsList mFavoriteData;
    private SavePrefs savePrefs;
    private Context mContext;
    private CarsRecyclerViewAdapter mCarsRecyclerViewAdapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = Objects.requireNonNull(getActivity()).getApplicationContext();
        savePrefs = new SavePrefs(mContext, CarsList.class);
        MobileAds.initialize(mContext,
                getString(R.string.app_ad_id));

        AdRequest adRequest = new AdRequest.Builder()/*.addTestDevice("C47FB4C7CA3EC78196EB786392DE443C")*/.build();
        mAdView.loadAd(adRequest);


        loadFavorites();
    }

    private void loadFavorites() {
        try {
            if (!Objects.equals(savePrefs.load(), null)) {
                mFavoriteData = new CarsList();
                mFavoriteData = ((CarsList) savePrefs.load());
                ArrayList<CarItem> tempList = new ArrayList<>(mFavoriteData.getCarItems());
                if (!tempList.isEmpty()) {
                    mCarsRecyclerView.setVisibility(View.VISIBLE);
                    mCarsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

                    mCarsRecyclerViewAdapter = new CarsRecyclerViewAdapter(mContext, tempList, true,"favorites");
//                    mCarsRecyclerViewAdapter.notifyDataSetChanged();
                    mCarsRecyclerView.setAdapter(mCarsRecyclerViewAdapter);
                }
            }
            mCarsProgressBar.setVisibility(View.GONE);
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "MainActivity 3: " + error.toString());
        }
    }


}
