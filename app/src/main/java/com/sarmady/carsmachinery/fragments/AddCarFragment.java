package com.sarmady.carsmachinery.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.sarmady.carsmachinery.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCarFragment extends Fragment {

    @BindView(R.id.ibtn_image_picker)
    ImageButton mImagePickerImageButton;
    @BindView(R.id.iv_new_car)
    ImageView mNewCarImageView;
    private ImagePicker imagePicker;


    public AddCarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_car, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImagePickerImageButton.setOnClickListener(v ->{
            refreshImagePicker();
            imagePicker.choosePicture(true);

        });

    }

    private void refreshImagePicker() {
        imagePicker = new ImagePicker(getActivity(),
                this,
                imageUri -> {/*on image picked */
                    mNewCarImageView.setImageURI(imageUri);
                })
                .setWithImageCrop(
                        3 /*aspect ratio x*/,
                        2 /*aspect ratio y*/);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.handleActivityResult(resultCode, requestCode, data);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePicker.handlePermission(requestCode, grantResults);
    }
}



