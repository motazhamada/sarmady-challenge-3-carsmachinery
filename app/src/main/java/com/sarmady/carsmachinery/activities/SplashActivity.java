package com.sarmady.carsmachinery.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.classes.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }, 3000);

    }
}
