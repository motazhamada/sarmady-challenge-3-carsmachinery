package com.sarmady.carsmachinery.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.classes.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity {

    private static final String EMAIL = "email";
    @BindView(R.id.btn_facebook_login)
    LoginButton mLoginButton;
    @BindView(R.id.btn_continue)
    Button mContinueButton;
    @BindView(R.id.iv_facebook_profile)
    ProfilePictureView mProfileImageView;
    @BindView(R.id.tv_facebook_username)
    TextView mUsernameTextView;
    @BindView(R.id.tv_facebook_email)
    TextView mEmailTextView;
    @BindView(R.id.pb_login)
    ProgressBar mLoginProgressBar;
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        // Initialize Facebook Login button

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        mLoginButton.setReadPermissions("email", "public_profile");
//        mLoginButton.setOnClickListener(v -> {
//            mLoginProgressBar.setVisibility(View.VISIBLE);
//        });
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mLoginProgressBar.setVisibility(View.VISIBLE);
                ProfileTracker profileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        this.stopTracking();
                        Profile.setCurrentProfile(currentProfile);
                    }
                };
                profileTracker.startTracking();
                Log.d(getString(R.string.TAG), "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
//                if(Profile.getCurrentProfile() == null) {
//                    mProfileTracker = new ProfileTracker() {
//                        @Override
//                        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//                            Log.v("facebook - profile", currentProfile.getFirstName());
//                            mProfileTracker.stopTracking();
//                        }
//                    };
//                    // no need to call startTracking() on mProfileTracker
//                    // because it is called by its constructor, internally.
//                }
//                else {
//                    Profile profile = Profile.getCurrentProfile();
//                    Log.v("facebook - profile", profile.getFirstName());
//                }

            }

            @Override
            public void onCancel() {
                Log.d(getString(R.string.TAG), "facebook:onCancel");
                // ...
                mLoginProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(getString(R.string.TAG), "facebook:onError", error);
                mLoginProgressBar.setVisibility(View.GONE);
                // ...
            }
        });
        mContinueButton.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            mUsernameTextView.setText(currentUser.getDisplayName());
            mEmailTextView.setText(currentUser.getEmail());
            if (Profile.getCurrentProfile() != null)
                mProfileImageView.setProfileId(Profile.getCurrentProfile().getId());
            mContinueButton.setVisibility(View.VISIBLE);
            mLoginButton.setVisibility(View.GONE);
        } else {
            mContinueButton.setVisibility(View.GONE);
            mLoginButton.setVisibility(View.VISIBLE);
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(getString(R.string.TAG), "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(getString(R.string.TAG), "signInWithCredential:success");

                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            mLoginProgressBar.setVisibility(View.GONE);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(getString(R.string.TAG), "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
