package com.sarmady.carsmachinery.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;

import com.sarmady.carsmachinery.R;
import com.sarmady.carsmachinery.classes.BaseActivity;
import com.sarmady.carsmachinery.fragments.AddCarFragment;
import com.sarmady.carsmachinery.fragments.FavoritesFragment;
import com.sarmady.carsmachinery.fragments.HomeFragment;
import com.sarmady.carsmachinery.fragments.MenuFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.bnv_cars_main)
    BottomNavigationView mBottomNavigationView;
    Fragment fragment;
//    MenuItem mPreviousItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        try {

            fragment = new HomeFragment();
            mBottomNavigationView.setOnNavigationItemSelectedListener(this);
            // Check that the activity is using the layout version with the fragment_container FrameLayout
            if (findViewById(R.id.fl_container) != null) {
                if (savedInstanceState != null) {
                    return;
                }
            }
//            fragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, fragment).commit();


        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "Error 5 : " + e.getMessage());
        }
    }

//
//    {
//        mPreviousItem = findViewById(R.id.action_home);
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                if (fragment instanceof HomeFragment)
                    return true;
                else
                    fragment = new HomeFragment();
                break;
            case R.id.action_gavel:
                if (fragment instanceof AddCarFragment)
                    return true;
                else
                    fragment = new AddCarFragment();
                break;
            case R.id.action_favorite:
                if (fragment instanceof FavoritesFragment)
                    return true;
                else
                    fragment = new FavoritesFragment();
                break;
            case R.id.action_menu:
                if (fragment instanceof MenuFragment)
                    return true;
                else
                    fragment = new MenuFragment();
                break;
            default:
                return true;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, fragment).commit();
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
                if (f instanceof AddCarFragment) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
}