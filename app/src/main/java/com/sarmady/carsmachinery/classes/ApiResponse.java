package com.sarmady.carsmachinery.classes;

public class ApiResponse {
    private long RefreshInterval;
    private long Ticks;
    public long getRefreshInterval() {
        return RefreshInterval;
    }

    public void setRefreshInterval(long refreshInterval) {
        RefreshInterval = refreshInterval;
    }

    public long getTicks() {
        return Ticks;
    }

    public void setTicks(long ticks) {
        Ticks = ticks;
    }

}
