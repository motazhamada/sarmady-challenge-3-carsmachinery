package com.sarmady.carsmachinery.classes;

import android.app.Application;
import android.content.Context;

import com.onesignal.OneSignal;

public class MainApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
//        LanguageHelper.setLanguage(this,"en");
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    // override the base context of application to update default locale for the application
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

}
